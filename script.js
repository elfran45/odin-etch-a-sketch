let grid = document.querySelector(".grid");
let resetBtn = document.querySelector(".resetBtn");
let input = document.querySelector(".input");
let inputBtn = document.querySelector(".input-btn");


function createGrid(columns) {

    for (j = 0; j < columns * columns; j++) {
        grid.style.setProperty('--grid-dimension', columns);
        let div = document.createElement("div");
        div.classList.add("square");
        div.addEventListener("mouseover", () => {
            div.classList.add("black-square");
        })
        grid.appendChild(div);
    }

}

createGrid(16);


function reset() {
    while (grid.firstChild) {
        grid.removeChild(grid.firstChild);
    }
    createGrid(input.value);
}

resetBtn.addEventListener("click", reset);

inputBtn.addEventListener('click', () => {
    let value = input.value;
    if (value <= 30) {
        createGrid(value);
    } else return;
});
